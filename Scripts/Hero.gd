extends Node

@export var wallet: int = 10
@export var inventory_size: int = 16
@export var health: int = 50
@export var max_health: int = 50
var is_inventory_full: bool = false : get = check_inventory_space
var isAlive: bool = true

# Called when the node enters the scene tree for the first time.
func _ready():
	$Inventory.load_list()

# Called every frame. 'delta' is the elapsed time since the previous frame.
func _process(_delta):
	pass

func add_to_inventory(item):
	#TODO: If stats better than equipped, equip. Else add to bag
	$Inventory.add_item(item)

func heal(amount = max_health):
	health = amount

func battle():
	# Generate a result. On success, hero triumphs in battle (and is rewarded with treasure)
	# On fail, take damage
	var fight_result = randi_range(0,1)
	if fight_result:
		return true
	else:
		health -= 1
		print(health)
	if health <= 0:
		isAlive = false
	return false

func check_inventory_space():
	var inventory_space = inventory_size - $Inventory.get_inventory_size()
	if inventory_space <= 0:
		return true
	else:
		return false
	
func subtract_from_wallet(amount):
	if(amount > wallet):
		push_error("Insufficient Funds")
	else:
		wallet -= amount

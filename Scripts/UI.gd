extends CanvasLayer

signal buy_item
@onready var gameManager = get_node("/root/Main/GameManager")
@onready var player = get_node("/root/Main/Player")
@onready var hero = get_node("/root/Main/Hero")

# Called when the node enters the scene tree for the first time.
func _ready():
	#refresh_screen()
	pass # Replace with function body.

# Called every frame. 'delta' is the elapsed time since the previous frame.
func _process(_delta):
	refresh_screen()
	pass

func refresh_screen():
	update_wallet()
	update_hero_wallet()
	update_inventory()
	update_hero_inventory()

func update_message(text):
	$Message.text = text
	$Message.show()

func update_inventory():
	$ShopInventory.text = parse_item_list(gameManager.list_shop_items(), "Sell")
	$ShopInventory.show()

func update_hero_inventory():
	if gameManager.is_hero_adventuring:
		$HeroInventory.text = "Hero is adventuring"
	else:
		$HeroInventory.text = parse_item_list(gameManager.list_hero_items(), "Buy")
	$HeroInventory.show()

func update_wallet():
	$Wallet.text = "Wallet: " + str(player.wallet) + " Coins"
	$Wallet.show()

func update_hero_wallet():
	$HeroWallet.text = "Hero's Wallet: " + str(hero.wallet) + " Coins"
	$HeroWallet.show()

func _on_buy_pressed():
	gameManager.buy_from_hero()
	gameManager.send_hero_adventuring()
	#buy_item.emit()

func parse_item_list(item_list, price_to_fetch):
	var return_string = ""
	for item in item_list:
		if price_to_fetch == "Buy":
			return_string += item.name + "   $ " + str(item.buy_price) + "\n"
		else:
			return_string += item.name + "   $ " + str(item.default_sell_price) + "\n"
	return return_string

extends Node

@export var is_hero_adventuring = false
@export var gold_rolls = [
	[0, 1],
	[1, 10],
	[15, 50],
	[50, 200],
	[100, 1000],
	[1000, 6000]
]

@onready var player = get_node("/root/Main/Player")
@onready var hero = get_node("/root/Main/Hero")
@onready var masterItemList = get_node("MasterItemList")
@onready var playerInventory = get_node("/root/Main/Player/Inventory")
@onready var heroInventory = get_node("/root/Main/Hero/Inventory")

# Called when the node enters the scene tree for the first time.
func _ready():
	pass

func random_item():
	return masterItemList.roll_item()

func rarity_to_text(rarity):
	return masterItemList.rarity_to_text(rarity)

func buy_from_hero():
	for item in heroInventory.all_items:
		# Make sure we can buy the item
		if item.buy_price <= player.wallet:
			# Remove gold from shop
			player.wallet -= item.buy_price
			# Add gold to hero
			hero.wallet += item.buy_price
			# Remove item from hero
			heroInventory.remove_item(item.name)
			# Trash all mundane items
			if item.rarity == 0.0:
				break
			# Add item to shop
			else:
				playerInventory.add_item(item)
	return

func sell_to_hero(item_to_sell):
	#remove gold from hero
	hero.wallet -= item_to_sell.default_sell_price
	#add gold to shop
	player.wallet += item_to_sell.default_sell_price
	#remove item from shop
	
	#add item to hero
	
	return

func roll_coins(rarity):
	#TODO Weight this to the lower end
	var amount = randi_range(gold_rolls[rarity][0], gold_rolls[rarity][1])
	return amount

func hero_adventure():
	var result = hero.battle()
	if result:
		var reward_item = random_item()
		var reward_coins = roll_coins(reward_item.rarity)
		heroInventory.add_item(reward_item)
		hero.wallet += reward_coins
	else:
		if !hero.isAlive:
			#game over
			is_hero_adventuring = false
	if hero.is_inventory_full:
		is_hero_adventuring = false

func list_hero_items():
	return heroInventory.all_items

func list_shop_items():
	return playerInventory.all_items

func send_hero_adventuring():
	hero.heal()
	is_hero_adventuring = true
	return

# Called every frame. 'delta' is the elapsed time since the previous frame.
func _process(_delta):
	if is_hero_adventuring:
		hero_adventure()
	pass

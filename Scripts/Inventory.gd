extends Node

class_name Inventory

@export var all_items = []
@export_file var item_list = ""
var inventory_size: int : get = get_inventory_size

func _ready():
	# Populate all_items
	load_list()
	pass

func get_inventory_size():
	return all_items.size()

func update_database():
	pass

func load_list():
	# Loads JSON Master Item List and parses it into an accessible array.
	var listFile = FileAccess.open(item_list, FileAccess.READ)
	var fileContents = listFile.get_as_text()
	var jsonObject = JSON.new()
	var parseError = jsonObject.parse(fileContents) # Check to make sure file parsed correctly
	if parseError == OK:
		all_items = jsonObject.get_data()
		if typeof(all_items) != TYPE_ARRAY:
			print("Unexpected data: ", all_items)
	else:
		print("JSON Parse Error: ", jsonObject.get_error_message(), " in ", listFile, " at line ", jsonObject.get_error_line())

func save_list():
	var listFile = FileAccess.open(item_list, FileAccess.WRITE)
	listFile.store_line(var_to_str(all_items))
	listFile = null

func find_item(item_name):
	# Find the item in the array and return its index.
	# Set default (false) item position. Change if found, otherwise stays false
	var item_index = -1
	# Search for the item by item_name. Save position if found.
	for item in all_items:
		if item.name == item_name:
			return all_items.find(item)
	
	return item_index

func get_item(item_name):
	# Get the actual item object from the array
	var item_position = find_item(item_name)
	if item_position >= 0:
		return all_items[item_position]
	else:
		push_error("Item not found")

func add_item(item):
	#TODO: Add quantities
#	var item_index = find_item(item.name)
#	if item_index >= 0:
#		if quantity in all_items[item_index]:
#			all_items[item_index]["quantity"] += quantity
#		else:
#			all_items[item_index]["quantity"] = quantity
#	else:
#		all_items.append(item)
	all_items.append(item)
	save_list()

func remove_item(item_name):
	#TODO: Add quantities
	# Remove item from our inventory. Throw error if item not found.
	# If we could not find item in all_items, throw an error
	var item_position = find_item(item_name)
	
	if item_position == -1:
		push_error("Item not found")
		return
	
	# Actually remove the item
	all_items.remove_at(item_position)
	save_list()

func _process(_delta):
# Called every frame. 'delta' is the elapsed time since the previous frame.
	pass

# Debug methods
func debug_print_all_items():
	for item in all_items:
		print(item)

func debug_item_removal():
	print("Removing Axe")
	remove_item("Axe")
	debug_print_all_items()
	print("Removing Axe (Again)")
	remove_item("Axe")
	debug_print_all_items()

extends Inventory

var rarities = [
	["Mundane",50.0],
	["Common",20.0],
	["Uncommon",15.0],
	["Rare",10.0],
	["Epic",4.8],
	["Legendary",0.2]
]

func rarity_to_text(rarity):
	return rarities[rarity][0]

func roll_rarity():
	# Roll a d1000 (so we can have 1 decimal place worth of rarity %)
	var roll = randi_range(0, 1000)
	
	# Convert that roll into a category
	var rarity = 0.0
	var threshold = 0.0
	for level in rarities:
		threshold += level[1]
		if roll <= (threshold * 10):
			return rarity
		rarity += 1.0
	return -1.0

func roll_item(rarity_num = roll_rarity()):
	# Get an item whose rarity is <= the rarity category we rolled
	var possible_rolls = []
	while true:
		# Filter all_items to just the ones that match our rarity target
		for item in all_items:
			if item.rarity == rarity_num:
				possible_rolls.append(item)
		# If there are no items at the rarity we rolled, include the rarity level below and try again
		if possible_rolls.is_empty():
			rarity_num -= 1
			if rarity_num < 0:
				# Should never happen, but breaks infinite loop
				return null
		else:
			break
	# Roll the actual item and return it.
	var index_to_return = randi_range(0, possible_rolls.size()-1)
	return possible_rolls[index_to_return]
